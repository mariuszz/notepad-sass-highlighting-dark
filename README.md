# Notepad++ Sass-Highlighter (dark theme)

Highlights Sass code in Notepad++ - yet rudimentary.

![Screenshot](https://bitbucket.org/mariuszz/notepad-sass-highlighting-dark/raw/6e1987c4365aa35a4ea9b8fff348f96a3bbebb17/notepad%2B%2B_sass_highlighter_2013-02-07.png)

----

**Required**
Dark (main) theme provided by Chris Kempson's [tomorrow_night](https://github.com/chriskempson/Tomorrow-Theme)

----

**Installation**

1. Clone/download 
2. At Notepad++ go to [Language]->[Define your language]->[Import]->(Open the xml file)->[Save As]
3. Restart or open a .sass file and change [Language] accordingly

----

**Kudos**
This theme is based on [Wouter Beugelsdijk's template](http://tech.wiedo.nl/sass-3-notepad-user-defined-syntax-highlighti).

Happy forking :)